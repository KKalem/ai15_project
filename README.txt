Demo
----

java -jar output/predict.jar 8000  # run this from the project's root folder

The "*" in the program's output indicates the preferred word completion choice
of our program.

Code Layout
-----------

src/ -> Word prediction engine

libs/ -> OpenNLP 1.6.0 libaries (make sure to use opennlp-tools-1.6.0.jar)
