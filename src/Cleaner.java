import java.util.ArrayList;
import java.util.StringTokenizer;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.InvalidFormatException;

import java.io.File;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import java.io.StringReader;

/**
 * GET FILE PATH AS A STRING, READ IT, TOKENIZE IT, RETURN A LIST OF WORDS
 */
public class Cleaner {

	static POSModel tmodel;
	static {
		tmodel = new POSModelLoader().load(new File("en-pos-maxent.bin"));
	}

	/**
	 * It converts the text in TXT to String
	 * This function just catch the content from txt file
	 *
	 * @param path the path of the corpus in TXT
	 * @return all the content of one corpus in a String
	 */
	public static String fromTXTtoString(String path) {

		String content = "";
		try {
			File file=new File(path);
			InputStreamReader read = new InputStreamReader(new FileInputStream(file));
			BufferedReader bufferedReader = new BufferedReader(read);
			String lineTxt = null;
			while((lineTxt = bufferedReader.readLine()) != null){
				content = content +"\n"+ lineTxt;
			}
			read.close();
		} catch (Exception e) {
			System.err.println("Error when reading the file!");
			e.printStackTrace();
		}
		return content;

	}

	// add one more parameters row, the number of rows of content in the corpus
	public static String fromTXTtoString(String path, int row) {

		String content = "";
		int count = 0;
		try {
			File file=new File(path);
			//Judge if the file exist
			InputStreamReader read = new InputStreamReader(new FileInputStream(file));
			BufferedReader bufferedReader = new BufferedReader(read);
			String lineTxt = null;
			while(((lineTxt = bufferedReader.readLine()) != null) && (count < row) ){
				content = content +"\n"+ lineTxt;
				count ++;
			}
			read.close();
		} catch (Exception e) {
			System.err.println("Error when reading the file!");
			e.printStackTrace();
		}
		return content;

	}

	/**
	 * It cut the string into sentences and add the ENDSENT in each sentense.
	 * Then add the word NOTAWORD as the end of this corpus
	 *
	 * @param content the content in String of the corpus in TXT
	 * @return all the sentences of one corpus in a String
	 */

	public static String SentenceDetect(String content, boolean tag, boolean addEndSent) throws InvalidFormatException,IOException {
		// always start with a model, a model is learned from training data
		InputStream is = new FileInputStream("en-sent.bin");
		SentenceModel model = new SentenceModel(is);
		SentenceDetectorME sdetector = new SentenceDetectorME(model);

		String sentences[] = sdetector.sentDetect(content);
		String str = "";

		if (addEndSent) {
			// add the word "ENDSENT" in the end of each sentense and store all sentense in a String
			for(int i=0; i<sentences.length; i++){
				sentences[i] = sentences[i] + " ENDSENT\n";
				str = str + sentences[i];
			}
		}

		is.close();

		return str.toLowerCase();
	}

	// this returns a collection of sentences
	public static String[] SentenceDetectMulti(String content) throws InvalidFormatException, IOException {
		// always start with a model, a model is learned from training data
		InputStream is = new FileInputStream("en-sent.bin");
		SentenceModel model = new SentenceModel(is);
		SentenceDetectorME sdetector = new SentenceDetectorME(model);
		String sentences[] = sdetector.sentDetect(content);

		/* add cleaning code here */
		for (int i = 0; i < sentences.length; i++)
		{
			String os = sentences[i];
			String cs = os.replaceAll("\\r?\\n", " ");
			sentences[i] = cs;
		}

		return sentences;
	}

	@SuppressWarnings("deprecation") // we don't want to mess with "charset" option
	public static String tagging(String sentence) throws IOException{
		POSTaggerME tagger = new POSTaggerME(tmodel);
		@SuppressWarnings("resource")
		ObjectStream<String> lineStream =
		new PlainTextByLineStream(new StringReader(sentence));
		String line;
		while ((line = lineStream.read()) != null) {
			String whitespaceTokenizerLine[] = WhitespaceTokenizer.INSTANCE.tokenize(line);
			String[] tags = tagger.tag(whitespaceTokenizerLine);

			POSSample sample = new POSSample(whitespaceTokenizerLine, tags);
			sentence = sample.toString();
		}

		return sentence;

	}


	/**
	 * It cut the string into words
	 * The input comes from the output of the SentenceDetect function
	 *
	 * @param content a String of the corpus after SentenceDetect operation
	 * @return all the words of one corpus in a ArrayList
	 */

	public static ArrayList<String> getWords(String content) {
		/*
		 *  TODO Return each word in the corpus as a list of strings.
		 *  Add "NOTAWORD" as a word to the end of sentences.
		 *  Ex: "Mary had a little lamb. It's name was jack." --> "mary" "had" "a" "little" "lamb" "ENDSENT" "its" "name" "was" "jack" "ENDSENT"
		 */

		StringTokenizer st =  new StringTokenizer(content);
		ArrayList <String> list = new ArrayList<String>();
		try{
			while(st.hasMoreTokens()){
				String word = st.nextToken();
				word = word.replaceAll("[^a-zA-Z_]", " ");
				StringTokenizer inner =  new StringTokenizer(word);
				while(inner.hasMoreTokens()){
					String innerword = inner.nextToken();
					list.add(innerword);
				}
			}
		} catch (Exception e) {
			System.err.println("Error when geting the words!");
			e.printStackTrace();
		}



		return list;
	}

	public static ArrayList<String> getWordsSafer(String content) {
		StringTokenizer st =  new StringTokenizer(content);
		ArrayList <String> list = new ArrayList<String>();
		try{
			while(st.hasMoreTokens()){
				String word = st.nextToken();
				list.add(word);
			}
		} catch (Exception e) {
			System.err.println("Error when geting the words!");
			e.printStackTrace();
		}

		return list;
	}

	// hack to force class loading at program startup
	public static void init() {

	}


}
