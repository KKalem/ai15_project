import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.imageio.ImageIO;

public class Util {

	@SuppressWarnings("unused")
	private static final boolean DEBUG = true;

	private static StringBuilder sb = new StringBuilder();

	public static void collectString(String s){
		sb.append(s);
	}

	public static void saveCollectedString(String path) throws FileNotFoundException{
		File log = new File(path+".txt");
		PrintWriter pfp = new PrintWriter(log);
		pfp.write(sb.toString());
		pfp.close();
	}

	/**
	 * A printer for debugging purposes, makes it easy to shut all printing off.
	 * Use this instead of syso'ing everything.
	 * @param s
	 * 		String to print
	 * @return
	 * 		Given string.
	 */
	public static final String debug(String s){
		collectString(s);
		return s;
	}

	public static final Number debug(Number n){
		debug(""+n);
		return n;
	}

	public static final char debug(char c) {
		debug(""+c);
		return c;
	}

	public static final void printSeperator(String title){
		debug("=============="+title+"==============\n");
	}

	public static void normalize(double[] a){
		double sum = 0;
		for(int i=0; i<a.length; i++){
			sum += Math.abs(a[i]);
		}
		for(int i=0; i<a.length; i++){
			a[i] = Math.abs(a[i])/sum;
		}
	}

	public static String d1DoubleArrayToStr(double[] a){
		StringBuilder sb = new StringBuilder();

		for(int j=0; j < a.length; j++){
			sb.append("  " + String.format("%2.5f", a[j]));
		}


		return sb.toString();

	}

	public static String d1IntArrayToStr(int[] a){
		StringBuilder sb = new StringBuilder();

		for(int j=0; j < a.length; j++){
			sb.append("  " + String.format("%d", a[j]));
		}


		return sb.toString();

	}

	public static String d2DoublArrayToStr(double[][] a){
		StringBuilder sb = new StringBuilder();

		for(int i=0; i < a.length; i++){
			sb.append("\n");

			for(int j=0; j < a.length; j++){
				sb.append("  " + String.format("%2.5f", a[i][j]));
			}
		}

		return sb.toString();
	}

	public static String d2IntArrayToStr(int[][] a){
		StringBuilder sb = new StringBuilder();

		for(int i=0; i < a[0].length; i++){
			sb.append("\n");

			for(int j=0; j < a[i].length; j++){
				sb.append("  " + String.format("%d", a[i][j]));
			}
		}

		return sb.toString();
	}

	public static void saveWordMapAsImage(Map<Tuple<Integer, Integer>, Integer> m, String filename) throws IOException{
		int width = Dictionary.getWordSize();
		int height = Dictionary.getWWSize();
		int colour = 255255;

		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for(int r=0; r<height; r++){
			for(int c=0; c<width; c++){
				Integer cell = m.get(new Tuple<Integer,Integer>(r, c));
				if(cell == null) cell = 0;
				bi.setRGB(c,r,(int) (colour* cell));
			}
		}


		ImageIO.write(bi, "PNG", new File(filename+".PNG"));
	}

	public static void saveCharMapAsImage(Map<Tuple<Integer, Integer>, Integer> m, String filename) throws IOException{
		int width = Dictionary.getCharSize();
		int height = Dictionary.getCCSize();
		int colour = 255255;

		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for(int r=0; r<height; r++){
			for(int c=0; c<width; c++){
				Integer cell = m.get(new Tuple<Integer,Integer>(r, c));
				if(cell == null) cell = 0;
				bi.setRGB(c,r,(int) (colour* cell));
			}
		}


		ImageIO.write(bi, "PNG", new File(filename+".PNG"));
	}

}
