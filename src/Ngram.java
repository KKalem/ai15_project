import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import opennlp.tools.ngram.NGramGenerator;

public class Ngram {

	//	TAKE DICTIONARY , N , AND CORPUS path AS INPUT
	//	COUNT THE WORDS IN THE DICTIONARY, THAT ARE INSIDE THE GIVEN CORPUS
	//	RECORD THE COUNTS IN THE CORPUS, IN A 2D ARRAY

	static String seperator = "-";
	private static final int n = 3;

	static Map<Tuple<Integer,Integer>,Integer> wordgramMat; // key tuple is the coordinates for the "matrix", value int is the count
	// Tuples are <word-word , word> formatted
	static Map<Tuple<Integer,Integer>,Integer> chargramMat; // same as wordgram

	static double wordMatDensity = 0.0;
	static double charMatDensity = 0.0;

	static Map<Integer, List<Tuple<Integer,Integer>>> reverseWordgramMat;

	/* POS ngrams */
	static ArrayList<List<String>> nGramsPOS;
	static HashMap<String, HashSet<String>> wordsToPOS = new HashMap<String, HashSet<String>>();
	static Set<String> AllPOS = new HashSet<String>();

	static Map<Tuple<List<String>,String> ,Integer> fouroneCountPosGrams;

	/* POS ngrams end */

	/* Initting methods */
	public static void init(){
		wordgramMat = new HashMap<Tuple<Integer,Integer>, Integer>();
		chargramMat = new HashMap<Tuple<Integer,Integer>, Integer>();
		reverseWordgramMat = new HashMap<Integer, List<Tuple<Integer,Integer>>>();
		fouroneCountPosGrams = new HashMap<Tuple<List<String>,String> ,Integer>();
	}

	public static List<String> getWord2Grams(ArrayList<String> wordList){
		return NGramGenerator.generate(wordList,2,seperator);
	}

	public static List<String> getChar2Grams(char[] chars){
		return NGramGenerator.generate(chars, 2, seperator);
	}


	private static void addWordGramToMat(String[] ngram){
		int r = Dictionary.getWWIndex(ngram[0],ngram[1]);
		int c = Dictionary.getWordIndex(ngram[2]);
		Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(r,c);
		Integer old = wordgramMat.get(key);
		if(old == null) { //first time seeing this 3gram, put a new key for it, set count to 1
			wordgramMat.put(key,1);
		}else{ // this 3-gram is already added, increase count only
			wordgramMat.put(key,old+1);
		}
	}


	private static void addCharGramToMat(String[] ngram){
		int r = Dictionary.getCCIndex(ngram[0],ngram[1]);
		int c = Dictionary.getCharIndex(ngram[2]);
		Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(r,c);
		Integer old = chargramMat.get(key);
		if(old == null) chargramMat.put(key,1);
		else{
			chargramMat.put(key,old+1);
		}
	}


	public static void populateWordGramMatrix(ArrayList<String> wordList){
		List<String> wordgrams = NGramGenerator.generate(wordList, n, seperator);

		for(String s:wordgrams){
			String[] words = s.split(seperator);
			addWordGramToMat(words);
		}
		wordMatDensity = wordgramMat.size()*1.0 / ((Dictionary.getWWSize()*Dictionary.getWordSize()));

		for(Entry<Tuple<Integer,Integer>, Integer> e : wordgramMat.entrySet()){
			Tuple<Integer,Integer> key = e.getKey();
			int cnt = e.getValue();
			if(reverseWordgramMat.containsKey(cnt)){ // a list for this count exists, just add this key to the list
				reverseWordgramMat.get(cnt).add(key);
			}else{ // a list for this count does not exist, create a new list, add this key to it
				List<Tuple<Integer,Integer>> list = new ArrayList<Tuple<Integer,Integer>>();
				list.add(key);
				reverseWordgramMat.put(cnt, list);
			}
		}


	}

	public static void populateCharGramMatrix(char[] charArr){
		ArrayList<String> charList = new ArrayList<String>();
		for(char c: charArr){
			charList.add(""+c);
		}
		List<String> chargrams = NGramGenerator.generate(charList, n, seperator);

		for(String s:chargrams){
			String[] chars = s.split(seperator);
			addCharGramToMat(chars);
		}
		charMatDensity = chargramMat.size()*1.0 / ((Dictionary.getCCSize()*Dictionary.getCharSize()));
	}

	/* GETTERS FOR VALUES IN THE MATRIX */
	public static int getWordgram(String w1, String w2, String w3){
		Integer ret = wordgramMat.get(new Tuple<>(Dictionary.getWWIndex(w1, w2),Dictionary.getWordIndex(w3)));
		if(ret==null) return 0;
		else return ret.intValue();
	}

	public static int getChargram(String c1, String c2, String c3){
		Integer ret = chargramMat.get(new Tuple<>(Dictionary.getCCIndex(c1, c2),Dictionary.getCharIndex(c3)));
		if(ret==null) return 0;
		else return ret.intValue();
	}


	/* SERIALIZERS */
	public static final String wordgramToString(){
		StringBuilder sb = new StringBuilder();
		double total = 0;
		for(Entry<Tuple<Integer,Integer>, Integer> e:wordgramMat.entrySet()){
			total++;
			//ww,w -> count
			String ww = Dictionary.getIndexWW(e.getKey().first());
			String w = Dictionary.getIndexWord(e.getKey().second());
			int c = e.getValue();
			sb.append(ww+" - "+ w + " = " + c + "\n");
		}
		sb.append("TOTAL: " + total + "  Density:"+ wordMatDensity + "\n");
		return sb.toString();
	}

	public static final String chargramToString(){
		StringBuilder sb = new StringBuilder();
		double total = 0;
		for(Entry<Tuple<Integer,Integer>, Integer> e:chargramMat.entrySet()){
			total++;
			//ww,w -> count
			String cc = Dictionary.getIndexCC(e.getKey().first());
			String c = Dictionary.getIndexChar(e.getKey().second());
			int cnt = e.getValue();
			sb.append(cc+" - "+ c + " = " + cnt + "\n");
		}
		sb.append("TOTAL: " + total + "  Density:"+ charMatDensity + "\n");
		return sb.toString();
	}

	public static final String reverseWordgramMatToString(){
		StringBuilder sb = new StringBuilder();
		for(Entry<Integer, List<Tuple<Integer,Integer>>> e : reverseWordgramMat.entrySet()){
			int count = e.getKey();
			if(count < 5) continue; //these lines are needlessly long. like 3 MILLION CHARS long lines for each at 20k corpus
			sb.append(count+" >>> ");
			List<Tuple<Integer,Integer>> l = e.getValue();
			for(Tuple<Integer,Integer> key :l){
				String ww = Dictionary.getIndexWW(key.first());
				String w = Dictionary.getIndexWord(key.second());
				sb.append("["+ww+seperator+w+"] , ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	/* FILE OPERATIONS */
	private static void saveWordGramsToFile(String filename) throws IOException{
		FileOutputStream fop = new FileOutputStream(new File(filename+"_"+"wordgrammat"));
		ObjectOutputStream objOut = new ObjectOutputStream(fop);
		objOut.writeObject(wordgramMat);
		objOut.close();
	}

	private static void saveCharGramsToFile(String filename) throws IOException{
		FileOutputStream fop = new FileOutputStream(new File(filename+"_"+"chargrammat"));
		ObjectOutputStream objOut = new ObjectOutputStream(fop);
		objOut.writeObject(chargramMat);
		objOut.close();
	}

	private static void saveRevWordgram(String filename) throws IOException{
		FileOutputStream fop = new FileOutputStream(new File(filename+"_"+"reversewordgrammat"));
		ObjectOutputStream objOut = new ObjectOutputStream(fop);
		objOut.writeObject(reverseWordgramMat);
		objOut.close();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void loadWordGramMatrix(String filename) throws IOException, ClassNotFoundException{
		FileInputStream fip = new FileInputStream(new File(filename+"_"+"wordgrammat"));
		ObjectInputStream objIn = new ObjectInputStream(fip);
		wordgramMat= (HashMap) objIn.readObject();
		objIn.close();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void loadCharGramMatrix(String filename) throws IOException, ClassNotFoundException{
		FileInputStream fip = new FileInputStream(new File(filename+"_"+"chargrammat"));
		ObjectInputStream objIn = new ObjectInputStream(fip);
		chargramMat= (HashMap) objIn.readObject();
		objIn.close();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void loadRevWordGramMatrix(String filename) throws IOException, ClassNotFoundException{
		FileInputStream fip = new FileInputStream(new File(filename+"_"+"reversewordgrammat"));
		ObjectInputStream objIn = new ObjectInputStream(fip);
		reverseWordgramMat= (HashMap) objIn.readObject();
		objIn.close();
	}



	public static void saveNgram(String filename) throws IOException{
		saveCharGramsToFile(filename);
		saveWordGramsToFile(filename);
		saveRevWordgram(filename);
	}

	public static void loadNgram(String filename) throws ClassNotFoundException, IOException{
		init();
		loadCharGramMatrix(filename);
		loadWordGramMatrix(filename);
		loadRevWordGramMatrix(filename);
	}


	/* POS nGrams generation, and other functions */

	public static void getPOSgrams(LinkedList<LinkedList<String>> all_words, LinkedList<LinkedList<String>> all_tags) {
		// We need these lists of lists to generate nice maps from word to pos, and from pos to word domain
		System.err.println("[^] Generating nGrams in POS domain!");
		for (int i = 0; i < all_words.size(); i++) {
			// XXX
			int l1 = all_words.get(i).size();
			int l2 = all_words.get(i).size();

			if (l1 != l2) {
				System.out.println("WTF!?");
			}
		}

		/* Make a MAP from words to possible POSes */
		for (int i = 0; i < all_words.size(); i++) {
			LinkedList<String> inner_words = all_words.get(i);
			LinkedList<String> inner_tags = all_tags.get(i);
			for (int j = 0; j < inner_words.size(); j++) {
				String word = inner_words.get(j);
				if (wordsToPOS.get(word) == null) {
					wordsToPOS.put(word, new HashSet<String>());
				}
				String tag = inner_tags.get(j);
				wordsToPOS.get(word).add(tag);
			}

		}

		// flatten all_tags as opennlp.tools.ngram.NGramGenerator expects a List<String> to generate an nGram
		ArrayList <String> all_tags_flat = new ArrayList<String>();
		for (int i = 0; i < all_tags.size(); i++) {
			LinkedList<String> inner = all_tags.get(i);
			for (int j = 0; j < inner.size(); j++) {
				String tag = inner.get(j);
				all_tags_flat.add(tag);
				AllPOS.add(tag);
			}
		}

		System.out.print("Possible POS: ");
		for(String pos: AllPOS) {
			System.out.print(pos + " / ");
		}
		System.out.println("");

		nGramsPOS = new ArrayList<List<String>>(12);
		// LinkedList<LinkedList<String>>();
		for (int i = 1; i <= 10; i++) {
			List<String> fff = NGramGenerator.generate(all_tags_flat, i, seperator);
			nGramsPOS.add(fff);
		}


		List<String> pos5grams = nGramsPOS.get(4);
		for(String gram : pos5grams){
			String[] gramArr = gram.split(seperator);
			List<String> fourGram = new ArrayList<String>();
			for(int i=0; i<4; i++) fourGram.add(gramArr[i]);
			String fifth = gramArr[4];
			Tuple<List<String>,String> key = new Tuple<List<String>,String>(fourGram,fifth); 
			if(fouroneCountPosGrams.containsKey(key)){
				int old = fouroneCountPosGrams.get(key);
				fouroneCountPosGrams.put(key, ++old);
			}else{
				fouroneCountPosGrams.put(key, 1);
			}
		}


		System.err.println("[^] nGrams in POS. We have " + nGramsPOS.size() + " models!");
	}
}
