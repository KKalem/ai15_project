import java.io.Serializable;

public class Tuple<O1, O2> implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -3370214214402506277L;
	private final O1 o1;
	private final O2 o2;

	public Tuple(O1 o1, O2 o2){
		this.o1 = o1;
		this.o2 = o2;
	}

	public O1 first(){
		return o1;
	}
	public O2 second(){
		return o2;
	}

	public String toString(){
		return "[" + o1.toString() + "," + o2.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((o1 == null) ? 0 : o1.hashCode());
		result = prime * result + ((o2 == null) ? 0 : o2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		Tuple other = (Tuple) obj;
		if (o1 == null) {
			if (other.o1 != null)
				return false;
		} else if (!o1.equals(other.o1))
			return false;
		if (o2 == null) {
			if (other.o2 != null)
				return false;
		} else if (!o2.equals(other.o2))
			return false;
		return true;
	}
}
