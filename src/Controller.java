import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.List;

import opennlp.tools.util.InvalidFormatException;

public class Controller {

	public static void createAndSaveModels(String filename, int lineCount, boolean saveLog, boolean saveBinary, boolean saveCharImage, boolean saveWordImage, boolean tag) throws InvalidFormatException, IOException{
		/* PRE-PROCESS CORPUS */
		// path is the relative address of txt file
		String len = lineCount+""; //to name the files
		String corpusPath = filename;

		System.err.println("Reading "+lineCount+" lines from file "+filename);
		//reading the content of txt file and saving it in a String
		String corpusContent = Cleaner.fromTXTtoString(corpusPath,lineCount);
		// adding the ENDSENT in the end of each sentence
		String sentence = Cleaner.SentenceDetect(corpusContent, false, true); // don't do POS tagging here but do ENDSENT


		/* CREATE THE SEQUENCES OF WORDS ANC CHARS */
		System.err.println("Seperating content");
		ArrayList <String> corpusWordSeq = Cleaner.getWords(sentence);

		// initialize the dictionary
		System.err.println("Initialising dictionary");
		Dictionary.initMaps();

		//add all words and chars from the cleaner into the dictionary.
		System.err.println("Bulk adding words and characters to dictionary");
		Dictionary.addAllWords(corpusWordSeq);
		//Dictionary.addAllChars(corpusCharSeq);


		/* CREATE NGRAMS */
		//generate the 2grams first
		System.err.println("Generating 2-grams");
		List<String> word2grams = Ngram.getWord2Grams(corpusWordSeq);
		// List<String> char2grams = Ngram.getChar2Grams(corpusCharSeq);

		System.err.println("Adding 2-grams to dicitonary");
		//add these 2grams to dict
		Dictionary.addAllWWs(word2grams);
		// Dictionary.addAllCCs(char2grams);

		//initialise ngram
		System.err.println("Initialising 3-gram matrices");
		Ngram.init();

		//generate the 3gram arrays
		System.err.println("Bulk adding into 3-gram matrices");
		Ngram.populateWordGramMatrix(corpusWordSeq);
		// Ngram.populateCharGramMatrix(corpusCharSeq);


		/* Create NGRAM in POS domain */
		LinkedList<LinkedList<String>> all_words = new LinkedList<LinkedList<String>>();
		LinkedList<LinkedList<String>> all_tags = new LinkedList<LinkedList<String>>();
		String sentences[] = Cleaner.SentenceDetectMulti(corpusContent);
		for (String s: sentences) {
			// clean this sentence
			// String []cleaned_words = Cleaner.getWords(s);
			// tag this sentence!
			String tagged_sentence = Cleaner.tagging(s);
			// System.err.println(tagged_sentence);

			// cut the tagged sentence into words!
			ArrayList<String> tagged_words = Cleaner.getWordsSafer(tagged_sentence);

			LinkedList<String> clean_words = new LinkedList<String>();
			LinkedList<String> clean_tags = new LinkedList<String>();

			for (String tw: tagged_words) {
				// seperate tagged word into (word, tag)
				// find "_" from the end of string!
				int idx;
				for (idx = tw.length() - 1; idx >= 0; idx--) {
					if (tw.charAt(idx) == '_')
						break;
				}
				clean_words.add(tw.substring(0, idx));
				clean_tags.add(tw.substring(idx));

				if (tw.substring(idx).equals("_")) {
					System.err.println("VVVVVVVV " + tw + " XXX " + s + " YYYY" + tagged_sentence + " ZZZ " + tw.substring(0, idx));
				}

			}

			all_words.add(clean_words);
			all_tags.add(clean_tags);
		}
		Ngram.getPOSgrams(all_words, all_tags); // [2-gram, 3-gram, 4-grams ... 10-grams] in POS domain, to decide?

		if(saveLog){
			/* PRINT EVERYTHING */
			System.err.println("Saving to log file  "+len+"_log.txt");
			//print the single words/chars
			Util.printSeperator("SINGLE WORDS/CHARS");
			Util.debug(Dictionary.wordDictToString(true));
			Util.debug(Dictionary.charDictToString(true));
			//print the 2grams
			Util.printSeperator("2GRAMS");
			Util.debug(Dictionary.wwDictToString(true));
			Util.debug(Dictionary.ccDictToString(true));
			//print the ngram matrices
			Util.printSeperator("3GRAM MATRICES");
			Util.printSeperator("WORD GRAMS");
			Util.debug(Ngram.wordgramToString());
			Util.printSeperator("CHAR 3GRAMS");
			Util.debug(Ngram.chargramToString());
			//print reverse wordgram lists
			Util.printSeperator("REVERSE LIST");
			Util.printSeperator("3grams");
			Util.debug(Ngram.reverseWordgramMatToString());
			Util.printSeperator("2grams");
			Util.debug(Dictionary.reverseWWcountsToString());
			Util.debug(Dictionary.reverseWordcountsToString());

			Util.saveCollectedString(len+"_log");
			System.err.println("Saved to log file  "+len+"_log.txt");
		}
		if(saveBinary){
			//finally, save the matrices for later use
			System.err.println("Saving binaries");
			Ngram.saveNgram(len);
			Dictionary.saveDict(len);
			//these files can be used to load up later, instead of calculating from scratch
		}
		if(saveCharImage){
			//save the matrices as fancy little png images.
			System.err.println("Saving characters to PNG file  "+len+"_charGramMatrix.txt");
			// Util.saveCharMapAsImage(Ngram.chargramMat, len+"_charGramMatrix");
			System.err.println("Saved characters to PNG file  "+len+"_charGramMatrix.txt");
		}
		if(saveWordImage){
			//save the matrices as fancy little png images.
			System.err.println("Saving words to PNG file  "+len+"_wordGramMatrix.txt");
			Util.saveWordMapAsImage(Ngram.wordgramMat, len+"_wordGramMatrix");
			System.err.println("Saved words to PNG files  "+len+"_wordGramMatrix.txt");
		}


		System.err.println("**DONE**");
	}

	public static void loadModels(String prefix) throws ClassNotFoundException, IOException{
		System.err.println("Loading from "+prefix+" prefixed files");
		Dictionary.loadDict(prefix);
		Ngram.loadNgram(prefix);
	}

	public static void predictExample(){
		//predict next word from this example
		System.err.println("Predicting next word (" +Predict.n_suggestions + " suggestions after AND THE): " + Arrays.toString(Predict.PredictionKatz("and", "the")));
	}
}
