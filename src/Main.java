import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Collections;
import opennlp.tools.util.InvalidFormatException;

public class Main {

	// use it to test the cleaner function of dividing words
	@SuppressWarnings("resource")
	public static void main(String[] args) throws InvalidFormatException, IOException, ClassNotFoundException {
		/* PRE-PROCESS CORPUS */
		// path is the relative address of txt file
		int lineCount = 8000;
		try{
			Integer input = Integer.parseInt(args[0]);
			lineCount = input.intValue();
		}catch(Exception e){
			System.err.println("ARGUMENT MUST BE A SINGLE INTEGER AS THE SIZE FOR TRAINING LENGTH, DEFAULTING TO 8000 LINES");
			lineCount = 8000;
		}

		String corpusPath = "natural.txt";
		boolean saveLog = true;
		boolean saveBinary = false;
		boolean tagSentence = false;
		boolean saveCharImage = false;
		boolean saveWordImage = false; //set to false for lineCount > 1000. Java runs out of heap space to store the png.
		boolean saveNewModel = true; // set to true when you want to generate a new model, false when you want to load an existing one
		int POS_PREDICTION_THRESHOLD = 4;

		//create a new model or load an existing one. this only needs to be done once.
		if(saveNewModel) Controller.createAndSaveModels(corpusPath, lineCount, saveLog, saveBinary, saveCharImage, saveWordImage, tagSentence);
		else Controller.loadModels(lineCount+"");

		Cleaner.init(); // force loading of POS model

		/* hack here to add a nice, fancy interface ;) */
		Scanner scanner = new Scanner(System.in);
		while (true) {
			// String sentence = System.console().readLine("> ");
			System.out.print("\n> ");
			String sentence = "";
			try {
				sentence = scanner.nextLine();
			} catch(Exception e) {
				System.exit(-1);
			}
			String word1;
			String word2;

			/* split input sentence into words */
			String []words = sentence.split(" ");
			if (words.length >= 2) {
				word1 = words[words.length - 2];
				word2 = words[words.length - 1];
			} else if (words.length < 2) {
				word1 = "endsent";
				word2 = words[words.length - 1];
			} else {
				word1 = "endsent";
				word2 = "endsent";
			}

			/* do we have more than POS_PREDICTION_THRESHOLD words? */
			if (words.length >= POS_PREDICTION_THRESHOLD) {
				// System.err.println("[>] Activating prediction using POS models!");
				String tagged_sentence = Cleaner.tagging(sentence);
				System.err.println("\n" + tagged_sentence + " <- tagged sentence");
				// cut the tagged sentence into words!
				ArrayList<String> tagged_words = Cleaner.getWordsSafer(tagged_sentence);
				LinkedList<String> clean_tags = new LinkedList<String>();
				for (String tw: tagged_words) {
					// seperate tagged word into (word, tag)
					// find "_" from the end of string!
					int idx = -1;
					for (idx = tw.length() - 1; idx >=0;  idx--) {
						if (tw.charAt(idx) == '_')
							break;
					}
					if (idx != -1)
						clean_tags.add(tw.substring(idx));
					else {
						// System.err.println(tw + " FFF");
					}
				}
				/* for (String t: clean_tags) {
				   System.err.println("TAG -> " + t);
				   } */

				/* We use a 5-gram POS, so we only use last 4 POS */
				LinkedList<String> used_tags = new LinkedList<String>();
				for (int i = clean_tags.size() - 1; i >= clean_tags.size() - 4; i--) {
					used_tags.add(clean_tags.get(i));
				}
				Collections.reverse(used_tags);

				/*or (String t: used_tags) {
				  System.err.println(t + " <- tag");
				  } */

				String predictionsBestEver[] = Predict.Prediction(word1, word2, used_tags, true, true);
				for (int k = 0; k < predictionsBestEver.length; k++) {
					String word = predictionsBestEver[k];
					String nword = word.replace("endsent", ".");
					if (k == 0) {
						System.err.println(nword + " <- POS based prediction *");
					}
					else {
						System.err.println(nword + " <- POS based prediction");
					}

				}

				System.err.println("..."); // spacer
			}


			String out[] = Predict.PredictionKatz(word1, word2);
			for (int k = 0; k < out.length; k++) {
				String word = out[k];
				String nword = word.replace("endsent", ".");
				if (k == 0) {
					System.err.println(nword + " <- nGrams based prediction *");
				}
				else {
					System.err.println(nword + " <- nGrams based prediction");
				}
			}
		}


	}
}
