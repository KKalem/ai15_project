import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
/*input the string
 * */
public class Dictionary {

	//	LOOK A THE list of words from cleaner FIND UNIQUE WORDS, HAVE A HASHMAP, PUT WORDS-INDICES


	private static HashMap<String,Integer> wordIndex; //for 1 word -> index
	private static HashMap<String,Integer> charIndex; //for 1 char -> index
	private static HashMap<String,Integer> wwIndex;   //for word-word -> index
	private static HashMap<String,Integer> ccIndex;   //for char-char -> index

	// for reverse lookups !
	// doesnt have to be hashmaps i guess, lists would do? not sure.
	private static HashMap<Integer, String> Indexword; //for index -> 1 word
	private static HashMap<Integer, String> Indexchar; //for index -> 1 char
	private static HashMap<Integer, String> Indexww;   //for index -> word-word
	private static HashMap<Integer, String> Indexcc;   //for index -> char-char

	static HashMap<String,Integer> wordCounts;
	static HashMap<String,Integer> charCounts;
	static HashMap<String,Integer> wwCounts;
	static HashMap<String,Integer> ccCounts;

	static HashMap<Integer, List<Integer>> reverseWWcounts;
	static HashMap<Integer, List<Integer>> reverseWordcounts;


	private static int wordCount = 0;
	private static int charCount = 0;
	private static int wwCount = 0;
	private static int ccCount = 0;

	@SuppressWarnings("rawtypes")
	private static HashMap[] allDicts;
	private static String[] dictNames = {
			"wordIndex",
			"charIndex",
			"wwIndex",
			"ccIndex",
			"Indexword",
			"Indexchar",
			"Indexww",
			"Indexcc",
			"wordCounts",
			"wwCounts",
			"reverseWWcounts",
	"reverseWordCounts"};

	public static void initMaps(){
		wordIndex = new HashMap<String,Integer>();
		charIndex = new HashMap<String,Integer>();
		wwIndex = new HashMap<String,Integer>();
		ccIndex = new HashMap<String,Integer>();

		Indexword = new HashMap<Integer,String>();
		Indexchar = new HashMap<Integer,String>();
		Indexww = new HashMap<Integer,String>();
		Indexcc = new HashMap<Integer,String>();

		wordCounts = new HashMap<String,Integer>();
		charCounts = new HashMap<String,Integer>();
		wwCounts = new HashMap<String,Integer>();
		ccCounts = new HashMap<String,Integer>();


		reverseWWcounts = new HashMap<Integer, List<Integer>>();
		reverseWordcounts = new HashMap<Integer, List<Integer>>();

		allDicts = new HashMap[12];
		allDicts[0] = wordIndex;
		allDicts[1] = charIndex;
		allDicts[2] = wwIndex;
		allDicts[3] = ccIndex;
		allDicts[4] = Indexword;
		allDicts[5] = Indexchar;
		allDicts[6] = Indexww;
		allDicts[7] = Indexcc;
		allDicts[8] = wordCounts;
		allDicts[9] = wwCounts;
		allDicts[10] = reverseWWcounts;
		allDicts[11] = reverseWordcounts;
	}

	/* Adder methods */
	private static void addWordToIndex(String s){
		if(wordCounts.containsKey(s)){
			int old = wordCounts.get(s);
			wordCounts.put(s, ++old);
		}else{
			wordCounts.put(s, 1);
		}
		if(wordIndex.containsKey(s)) return;
		wordIndex.put(s, wordCount);
		Indexword.put(wordCount, s);
		wordCount++;
	}

	private static void addCharToIndex(String s){
		if(charCounts.containsKey(s)){
			int old = charCounts.get(s);
			charCounts.put(s, ++old);
		}else{
			charCounts.put(s, 1);
		}
		if(charIndex.containsKey(s)) return;
		charIndex.put(s, charCount);
		Indexchar.put(charCount, s);
		charCount++;
	}

	// ngram generator returns nragms in a single string anyways, no need to split etc
	public static void addWWToIndex(String ww){
		if(wwCounts.containsKey(ww)){
			int old = wwCounts.get(ww);
			wwCounts.put(ww, ++old);
		}else{
			wwCounts.put(ww, 1);
		}
		if(wwIndex.containsKey(ww)) return;
		wwIndex.put(ww, wwCount);
		Indexww.put(wwCount, ww);
		wwCount++;
	}

	public static void addCCToIndex(String cc){
		if(ccCounts.containsKey(cc)){
			int old = ccCounts.get(cc);
			ccCounts.put(cc, ++old);
		}else{
			ccCounts.put(cc, 1);
		}
		if(ccIndex.containsKey(cc)) return;
		ccIndex.put(cc, ccCount);
		Indexcc.put(ccCount, cc);
		ccCount++;
	}

	/* Getter methods, will throw a NPE if the given char/word does not exist in the dictionary.
	 * It ALWAYS should exist here first before anywhere else. */
	public static int getWordIndex(String s){
		Integer r = 0;
		r = wordIndex.get(s);
		if(r==null) return -1;
		else return r.intValue();
	}

	public static int getCharIndex(String s){
		Integer r = 0;
		r = charIndex.get(s);
		if(r==null) return -1;
		else return r.intValue();
	}

	public static int getWWIndex(String s1, String s2){
		Integer r = 0;
		r = wwIndex.get(s1+Ngram.seperator+s2);
		if(r == null) return -1;
		else return r.intValue();
	}

	public static int getCCIndex(String s1, String s2){
		Integer r = 0;
		r = ccIndex.get(s1+Ngram.seperator+s2);
		if(r == null) return -1;
		else return r.intValue();
	}
	//COUNTS
	public static int getWordCount(String w){
		Integer r = 0;
		r = wordCounts.get(w);
		if(r == null) return 0;
		else return r.intValue();
	}

	public static int getCharCount(String w){
		Integer r = 0;
		r = charCounts.get(w);
		if(r == null) return 0;
		else return r.intValue();
	}

	public static int getWWCount(String w1, String w2){
		Integer r = 0;
		r = wwCounts.get(w1+Ngram.seperator+w2);
		if(r == null) return 0;
		else return r.intValue();
	}

	private static Integer getWWCount(String value) {
		Integer r = 0;
		r = wwCounts.get(value);
		if(r == null) return 0;
		else return r.intValue();
	}

	public static int getCCCount(String w1, String w2){
		Integer r = 0;
		r = ccCounts.get(w1+Ngram.seperator+w2);
		if(r == null) return 0;
		else return r.intValue();
	}

	//reverse getters to get the word given its index
	public static String getIndexWord(int i){
		return Indexword.get(i);
	}

	public static String getIndexChar(int i){
		return Indexchar.get(i);
	}

	public static String getIndexWW(int i){
		return Indexww.get(i);
	}

	public static String getIndexCC(int i){
		return Indexcc.get(i);
	}

	/* bulk adds. for use with the output from Cleaner */
	public static void addAllWords(ArrayList<String> list){
		for(String s : list){
			addWordToIndex(s);
		}
		for(Entry<String, Integer> e : wordCounts.entrySet()){
			String ww = e.getKey();
			int count = e.getValue();
			int wwi = wordIndex.get(ww);
			if(reverseWordcounts.containsKey(count)){ // a list for this count exists, just add this key to the list
				reverseWordcounts.get(count).add(wwi);
			}else{ // a list for this count does not exist, create a new list, add this key to it
				List<Integer> ls = new ArrayList<Integer>();
				ls.add(wwi);
				reverseWordcounts.put(count, ls);
			}
		}
	}

	public static void addAllChars(char[] chars){
		for(char c : chars){
			addCharToIndex(c+"");
		}
	}

	public static void addAllCCs(List<String> list){
		for(String s : list){
			addCCToIndex(s);
		}
	}

	public static void addAllWWs(List<String> list){
		for(String s: list){
			addWWToIndex(s);
		}
		for(Entry<String, Integer> e : wwCounts.entrySet()){
			String ww = e.getKey();
			int count = e.getValue();
			int wwi = wwIndex.get(ww);
			if(reverseWWcounts.containsKey(count)){ // a list for this count exists, just add this key to the list
				reverseWWcounts.get(count).add(wwi);
			}else{ // a list for this count does not exist, create a new list, add this key to it
				List<Integer> ls = new ArrayList<Integer>();
				ls.add(wwi);
				reverseWWcounts.put(count, ls);
			}
		}
	}


	/* Serialization
	 * These return a string representation of the entire dictionary. */
	public static String wordDictToString(boolean reverse){
		StringBuilder sb =  new StringBuilder();
		if(reverse){
			sb.append("Index\tWord\tCount\n");
			for(Entry<Integer, String> e : Indexword.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\t"+getWordCount(e.getValue()) +"\n");
			}
			sb.append("*TOTAL*\t" + wordCount +"\n");
			return sb.toString();
		}else{
			sb.append("Word\tIndex\n");
			for(Entry<String, Integer> e : wordIndex.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\t"+getWordCount(e.getKey()) +"\n");
			}
			sb.append("*TOTAL*\t" + wordCount +"\n");
			return sb.toString();
		}
	}


	public static String charDictToString(boolean reverse) {
		StringBuilder sb =  new StringBuilder();
		if(reverse){
			sb.append("Index\tChar\n");
			for(Entry<Integer, String> e : Indexchar.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\n");
			}
			sb.append("*TOTAL*\t" + charCount +"\n");
			return sb.toString();
		}else{
			sb.append("Char\tIndex\n");
			for(Entry<String, Integer> e : charIndex.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\n");
			}
			sb.append("*TOTAL*\t" + charCount +"\n");
			return sb.toString();
		}
	}

	public static String wwDictToString(boolean reverse){
		StringBuilder sb =  new StringBuilder();
		if(reverse){
			sb.append("Index\tWord-Word\tCount\n");
			for(Entry<Integer, String> e : Indexww.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\t"+getWWCount(e.getValue()) +"\n");
			}
			sb.append("*TOTAL*\t" + wwCount +"\n");
			return sb.toString();
		}else{
			sb.append("Wordword\tIndex\tCount\n");
			for(Entry<String, Integer> e : wwIndex.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\t"+getWWCount(e.getKey()) +"\n");
			}
			sb.append("*TOTAL*\t" + wwCount +"\n");
			return sb.toString();
		}
	}



	public static String ccDictToString(boolean reverse){
		StringBuilder sb =  new StringBuilder();
		if(reverse){
			sb.append("Index\tChar-Char\n");
			for(Entry<Integer, String> e : Indexcc.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\n");
			}
			sb.append("*TOTAL*\t" + ccCount +"\n");
			return sb.toString();
		}else{
			sb.append("Charchar\tIndex\n");
			for(Entry<String, Integer> e : ccIndex.entrySet() ){
				sb.append(e.getKey()+"\t"+e.getValue()+"\n");
			}
			sb.append("*TOTAL*\t" + ccCount +"\n");
			return sb.toString();
		}
	}


	public static String reverseWWcountsToString(){
		StringBuilder sb = new StringBuilder();
		for(Entry<Integer, List<Integer>> e : reverseWWcounts.entrySet()){
			int count = e.getKey();
			//			if(count < 5) continue; //these lines are needlessly long. like 3 MILLION CHARS long lines for each at 20k corpus
			sb.append(count+" >>> ");
			List<Integer> l = e.getValue();
			for(Integer key :l){
				String ww = Indexww.get(key);
				sb.append("["+ww+"] , ");
			}
			sb.append("\n");
		}
		return sb.toString();

	}

	public static String reverseWordcountsToString(){
		StringBuilder sb = new StringBuilder();
		for(Entry<Integer, List<Integer>> e : reverseWordcounts.entrySet()){
			int count = e.getKey();
			//			if(count < 5) continue; //these lines are needlessly long. like 3 MILLION CHARS long lines for each at 20k corpus
			sb.append(count+" >>> ");
			List<Integer> l = e.getValue();
			for(Integer key :l){
				String ww = Indexword.get(key);
				sb.append("["+ww+"] , ");
			}
			sb.append("\n");
		}
		return sb.toString();

	}

	/* SAVING AND LOADING THE DICT TO/FROM FILE */
	//seems to be working, not sure if the problem is here or at loading
	public static void saveDict(String filename) throws IOException{
		for(int i=0; i<allDicts.length; i++){
			FileOutputStream fop = new FileOutputStream(new File(filename+"_"+dictNames[i]));
			ObjectOutputStream objOut = new ObjectOutputStream(fop);
			objOut.writeObject(allDicts[i]);
			objOut.close();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void loadDict(String filename) throws ClassNotFoundException, IOException{
		initMaps();
		for(int i=0; i<allDicts.length; i++){
			FileInputStream fip = new FileInputStream(new File(filename+"_"+dictNames[i]));
			ObjectInputStream objIn = new ObjectInputStream(fip);
			allDicts[i] = (HashMap) objIn.readObject();
			objIn.close();
		}
		wordIndex = allDicts[0];
		charIndex = allDicts[1];
		wwIndex  = 	allDicts[2];
		ccIndex  = 	allDicts[3];
		Indexword = allDicts[4];
		Indexchar = allDicts[5];
		Indexww  = 	allDicts[6];
		Indexcc  = 	allDicts[7];
		wordCounts = allDicts[8];
		wwCounts = allDicts[9];
		reverseWWcounts = allDicts[10];
		reverseWordcounts = allDicts[11];


		wordCount = wordIndex.size();
		charCount = charIndex.size();
		ccCount = ccIndex.size();
		wwCount = wwIndex.size();
	}

	/* Properties of dictionary */
	public static int getWordSize(){ return wordCount; }
	public static int getCharSize(){ return charCount; }
	public static int getCCSize(){ return ccCount; }
	public static int getWWSize(){ return wwCount; }

}
