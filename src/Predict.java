import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

public class Predict {

	public static final int n_suggestions = 10; //Number of suggested words for every prediction
	public static final int par_k = 5;

	/**
	 * The function predicts the next word (SMOOTHING TECHNIQUE: ADDITIVE SMOOTHING WITH GAMMA=1)
	 *
	 * @param str1 str1 and str2 form the 2-gram (history from the user)
	 * @param str2 str1 and str2 form the 2-gram (history from the user)
	 * @return a list of suggested words in descending order of probability
	 */
	public static String[] PredictionAdd(String str1,String str2) {
		String[] WordList = new String[n_suggestions];
		int[] CountList = new int[n_suggestions];
		int Size = 0;
		Random random = new Random();

		try{

			int N = Dictionary.getWordSize();               //Size of the dict
			int WWIndex = Dictionary.getWWIndex(str1,str2); //Index of 2-gram
			Integer NGramCount = 0;							//Count of the 3-gram


			//Firstly, consider only 3-grams that appear in the corpus
			for(int i = 0; i < N; i++){
				String PWord = Dictionary.getIndexWord(i); //Third word
				Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,i);
				NGramCount = Ngram.wordgramMat.get(key);

				//If the 3-gram exists in corpus
				if (NGramCount != null){
					//WordList is not filled -> it keeps adding words
					if (Size < n_suggestions) {
						WordList[Size] = PWord;
						CountList[Size] = NGramCount;
						Size = Size + 1;
						//WordList is filled -> it prioritizes words that have a higher frequency in the corpus
					} else {
						//Select the minimum value in CountList
						int Min = Integer.MAX_VALUE;
						int MinIndex = 0;
						for(int j = 0; j < n_suggestions; j++){
							if (CountList[j] < Min) {
								Min = CountList[j];
								MinIndex = j;
							}
						}
						//Replace if count is higher than Min
						//If it is equal, than it will randomly decide to replace the word or not
						if (NGramCount > Min || (NGramCount == Min && random.nextBoolean())) {
							CountList[MinIndex] = NGramCount;
							WordList[MinIndex] = PWord;
						}
					}
				}
			}

			//WordList is not filled -> consider 3-grams that never appeared in the corpus
			//Here it picks the words randomly in the dictionary (their 3-grams have count zero in ngram matrix, so they can be randomly chosen)
			while (Size < n_suggestions) {
				int DictIndex = random.nextInt(N);
				String PWord = Dictionary.getIndexWord(DictIndex); //Third word
				Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,DictIndex);
				NGramCount = Ngram.wordgramMat.get(key);

				if (NGramCount == null){
					WordList[Size] = PWord;
					CountList[Size] = 0;
					Size = Size + 1;
				}
			}


			//WordList in ascending order of counts
			String[] WordList_sort = new String [n_suggestions];
			int[] CountList_sort = new int[n_suggestions];

			for(int i = 0; i < n_suggestions; i++){
				WordList_sort[i] = WordList[i];
				CountList_sort[i] = CountList[i];
			}
			Arrays.sort(CountList_sort);
			for(int i = 0; i < n_suggestions; i++){
				for(int j = 0; j < n_suggestions; j++){
					if(CountList_sort[i] == CountList[j]){
						WordList_sort[i] = WordList[j];
						CountList[j] = -1;
						break;
					}
				}
			}

			//WordList in descending order of counts
			for(int i = 0; i < n_suggestions; i++){
				WordList[i] = WordList_sort[n_suggestions-1-i];
			}

		} catch (Exception e) {
			System.out.println("Error when predicting next word!");
			e.printStackTrace();
		}

		//The list returns the suggested words
		//It is not necessary to use probability here-_-!
		return WordList;
	}

	/**
	 * The function predicts the next word (SMOOTHING TECHNIQUE: KATZ SMOOTHING WITH K=5)
	 *
	 * @param str1 str1 and str2 form the 2-gram (history from the user)
	 * @param str2 str1 and str2 form the 2-gram (history from the user)
	 * @return a list of suggested words in descending order of probability
	 */
	public static String[] PredictionKatz(String str1,String str2) {
		String[] WordList = new String[n_suggestions];
		double[] ProbList = new double[n_suggestions];
		double ProbWord;
		double Alpha2_SumProbNum = 0;				  //Elements used to compute Alpha
		double Alpha2_SumProbDen = 0;
		double Alpha1_SumProbNum = 0;
		double Alpha1_SumProbDen = 0;
		int Size = 0;
		double Min;
		int MinIndex;
		Random random = new Random();
		int CountWDict = Dictionary.getWordSize();    //Size of the word dictionary
		int WCountW2 = Dictionary.getWordCount(str2); //Count of str2
		double N = 0;								  //Total number of words in corpus

		try {

			int WWIndex = Dictionary.getWWIndex(str1,str2);
			//IF THERE ARE 3-GRAMS
			if (WWIndex != -1) {
				double nk1_3gram = Ngram.reverseWordgramMat.get(par_k + 1).size();
				double n1_3gram = Ngram.reverseWordgramMat.get(1).size();
				int WWCount = Dictionary.getWWCount(str1, str2);

				//Go through the dictionary searching for 3-grams
				for(int i = 0; i < CountWDict; i++){
					String PWord = Dictionary.getIndexWord(i); //Third word
					Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,i);
					Integer NGramCount = Ngram.wordgramMat.get(key);

					//If the 3-gram exists in corpus
					if (NGramCount != null){
						//WordList is not filled -> it keeps adding words
						if (Size < n_suggestions) {
							WordList[Size] = PWord;
							ProbWord = calc_d(3,NGramCount,nk1_3gram,n1_3gram)*(((double)NGramCount)/WWCount);
							ProbList[Size] = ProbWord;
							Size = Size + 1;

							Alpha2_SumProbNum = Alpha2_SumProbNum + ProbWord;
							int WWCountW2W3 = Dictionary.getWWCount(str2, Dictionary.getIndexWord(i));
							Alpha2_SumProbDen = Alpha2_SumProbDen + calc_d(2,WWCountW2W3,nk1_3gram,n1_3gram)*(WWCountW2W3/WCountW2);
						} else {
							ProbWord = calc_d(3,NGramCount,nk1_3gram,n1_3gram)*(((double)NGramCount)/WWCount);

							Alpha2_SumProbNum = Alpha2_SumProbNum + ProbWord;
							int WWCountW2W3 = Dictionary.getWWCount(str2, Dictionary.getIndexWord(i));
							Alpha2_SumProbDen = Alpha2_SumProbDen + calc_d(2,WWCountW2W3,nk1_3gram,n1_3gram)*(((double)WWCountW2W3)/WCountW2);

							//Select the minimum prob in ProbList
							Min = 1000.0;
							MinIndex = 0;
							for(int j = 0; j < n_suggestions; j++){
								if (ProbList[j] < Min) {
									Min = ProbList[j];
									MinIndex = j;
								}
							}
							//Replace if prob is higher than Min
							//If it is equal, than it will randomly decide to replace the word or not
							if (ProbWord > Min || (ProbWord == Min && random.nextBoolean())) {
								ProbList[MinIndex] = ProbWord;
								WordList[MinIndex] = PWord;
							}
						}
					}
				}
			}

			//The counts subtracted from the nonzero counts are distributed among the zero-count (this is the role of Alpha)
			if (Alpha2_SumProbDen == 1.0) Alpha2_SumProbDen = 0.95;
			double Alpha2 = (1 - Alpha2_SumProbNum)/(1 - Alpha2_SumProbDen);

			//Calculating N
			for(Entry<Integer, List<Integer>> e : Dictionary.reverseWordcounts.entrySet() ){
				N = N + e.getKey()*e.getValue().size();
			}
			double nk1_1gram = Dictionary.reverseWordcounts.get(par_k + 1).size();
			double n1_1gram = Dictionary.reverseWordcounts.get(1).size();

			if (Size < n_suggestions) {
				int WIndexW2 = Dictionary.getWordIndex(str2);
				//IF THERE ARE 2-GRAMS
				if (WIndexW2 != -1) {
					double nk1_2gram = Dictionary.reverseWWcounts.get(par_k + 1).size();
					double n1_2gram = Dictionary.reverseWWcounts.get(1).size();

					//Go through the dictionary searching for 2-grams
					for(int i = 0; i < CountWDict; i++){
						String PWord = Dictionary.getIndexWord(i); //Third word
						int NGramCount_int = Dictionary.getWWCount(str2, PWord);

						//If the 2-gram exists in corpus
						if (NGramCount_int != 0){
							//WordList is not filled -> it keeps adding words
							if (Size < n_suggestions) {
								ProbWord = calc_d(2,NGramCount_int,nk1_2gram,n1_2gram)*(((double)NGramCount_int)/WCountW2);

								Alpha1_SumProbNum = Alpha1_SumProbNum + ProbWord;
								int WCountW3 = Dictionary.getWordCount(PWord);
								Alpha1_SumProbDen = Alpha1_SumProbDen + calc_d(1,WCountW3,nk1_1gram,n1_1gram)*(((double)WCountW3)/N);

								if (wordIsNew(WordList,PWord)){
									WordList[Size] = PWord;
									ProbList[Size] = Alpha2*ProbWord;
									Size = Size + 1;
								}
							} else {
								ProbWord = calc_d(2,NGramCount_int,nk1_2gram,n1_2gram)*(((double)NGramCount_int)/WCountW2);

								Alpha1_SumProbNum = Alpha1_SumProbNum + ProbWord;
								int WCountW3 = Dictionary.getWordCount(PWord);
								Alpha1_SumProbDen = Alpha1_SumProbDen + calc_d(1,WCountW3,nk1_1gram,n1_1gram)*(((double)WCountW3)/N);

								if (wordIsNew(WordList,PWord)){
									//Select the minimum prob in ProbList
									Min = 1000.0;
									MinIndex = 0;
									for(int j = 0; j < n_suggestions; j++){
										if (ProbList[j] < Min) {
											Min = ProbList[j];
											MinIndex = j;
										}
									}
									//Replace if prob is higher than Min
									//If it is equal, than it will randomly decide to replace the word or not
									if ((Alpha2*ProbWord) > Min || ((Alpha2*ProbWord) == Min && random.nextBoolean())) {
										ProbList[MinIndex] = Alpha2*ProbWord;
										WordList[MinIndex] = PWord;
									}
								}
							}
						}
					}
				}
			}

			//The counts subtracted from the nonzero counts are distributed among the zero-count (this is the role of Alpha)
			if (Alpha1_SumProbDen == 1.0) Alpha1_SumProbDen = 0.95;
			double Alpha1 = (1 - Alpha1_SumProbNum)/(1 - Alpha1_SumProbDen);

			if (Size < n_suggestions) {
				//Go through the dictionary searching for 1-grams
				for(int i = 0; i < CountWDict; i++){
					String PWord = Dictionary.getIndexWord(i); //Third word
					int NGramCount_int = Dictionary.getWordCount(PWord);
					//WordList is not filled -> it keeps adding words
					if (Size < n_suggestions) {
						if (wordIsNew(WordList,PWord)){
							WordList[Size] = PWord;
							ProbWord = Alpha1 * calc_d(1,NGramCount_int,nk1_1gram,n1_1gram)*(((double)NGramCount_int)/N);
							ProbList[Size] = ProbWord;
							Size = Size + 1;
						}
					} else {
						if (wordIsNew(WordList,PWord)){
							ProbWord = Alpha1 * calc_d(1,NGramCount_int,nk1_1gram,n1_1gram)*(((double)NGramCount_int)/N);

							//Select the minimum prob in ProbList
							Min = 1000.0;
							MinIndex = 0;
							for(int j = 0; j < n_suggestions; j++){
								if (ProbList[j] < Min) {
									Min = ProbList[j];
									MinIndex = j;
								}
							}
							//Replace if prob is higher than Min
							//If it is equal, than it will randomly decide to replace the word or not
							if (ProbWord > Min || (ProbWord == Min && random.nextBoolean())) {
								ProbList[MinIndex] = ProbWord;
								WordList[MinIndex] = PWord;
							}
						}
					}
				}
			}

			//WordList in ascending order of counts
			String[] WordList_sort = new String [n_suggestions];
			double[] ProbList_sort = new double[n_suggestions];

			for(int i = 0; i < n_suggestions; i++){
				WordList_sort[i] = WordList[i];
				ProbList_sort[i] = ProbList[i];
			}
			Arrays.sort(ProbList_sort);
			for(int i = 0; i < n_suggestions; i++){
				for(int j = 0; j < n_suggestions; j++){
					if(ProbList_sort[i] == ProbList[j]){
						WordList_sort[i] = WordList[j];
						ProbList[j] = -1;
						break;
					}
				}
			}

			//WordList in descending order of counts
			for(int i = 0; i < n_suggestions; i++){
				WordList[i] = WordList_sort[n_suggestions-1-i];
			}

		} catch (Exception e) {
			System.out.println("Error when predicting next word!");
			e.printStackTrace();
		}

		return WordList;
	}

	//It calculates the discount ratio
	public static double calc_d(int ngram, int counts, double nk1, double n1) {
		double calc;
		double interm;
		int count = counts;
		if (count > par_k) {
			calc = 1.0;
		} else {
			interm = ((par_k + 1)*nk1)/n1;
			calc = ((r_star(ngram,count)/count) - interm)/(1 - interm);
		}
		return calc;
	}

	//It calculates the modified count for an n-gram
	public static double r_star(int ngram, int r) {
		double calc;
		Integer keym = r;
		double nr;
		double nr1;
		if (ngram == 3) {
			nr1 = Ngram.reverseWordgramMat.get(keym+1).size();
			nr = Ngram.reverseWordgramMat.get(keym).size();
			calc = (r + 1) * (nr1/nr);
		} 	else {
			if (ngram == 2) {
				nr1 = Dictionary.reverseWWcounts.get(keym+1).size();
				nr = Dictionary.reverseWWcounts.get(keym).size();
				calc = (r + 1) * (nr1/nr);
			} else {
				nr1 = Dictionary.reverseWordcounts.get(keym+1).size();
				nr = Dictionary.reverseWordcounts.get(keym).size();
				calc = (r + 1) * (nr1/nr);
			}
		}
		return calc;
	}

	//It checks if the word is already in the vector WordList or not
	public static boolean wordIsNew(String[] vector, String word) {
		boolean answer = true;
		for(int j = 0; j < n_suggestions; j++){
			if (vector[j] == word) answer = false;
		}
		return answer;
	}

	/**
	 * This function controls the Prediction
	 *
	 * @param str1 str1 and str2 form the 2-gram (history from the user)
	 * @param str2 str1 and str2 form the 2-gram (history from the user)
	 * @param POS_tags the POS Tags of the 4 last words
	 * @param withPOS true if the prediction should include POS, false otherwise
	 * @param withKatz true if the smoothing technique is Katz, false if the
	 * @return a list of suggested words in descending order of probability
	 */
	public static String[] Prediction(String str1, String str2, List<String> POS_tags, boolean withPOS, boolean withKatz) {
		String[] result = new String[10];
		if (withPOS) {
			//GO FIND THE MOST PROBABLE NEXT POS
			//Go through all the possible POS
			int MaxCount = 0;
			String NextPOS = null;
			for(String x: Ngram.AllPOS){
				Tuple<List<String>, String> key = new Tuple<List<String>, String>(POS_tags, x);
				Integer POS_Count = Ngram.fouroneCountPosGrams.get(key);

				if (POS_Count != null){
					if (POS_Count > MaxCount) {
						MaxCount = POS_Count;
						NextPOS = key.second();
					}
				}
			}
			System.out.println("\n<Next POS seem to be " + NextPOS + ">\n");

			//If I found a next POS
			if (NextPOS != null) {
				if (withKatz) {
					result = PredictionKatzPOS(str1,str2,NextPOS); //New prediction with Katz
				} else {
					result = PredictionAddPOS(str1,str2,NextPOS); //New prediction with Add
				}
				//If I did not find a next POS -> Predict without POS
			} else {
				if (withKatz) {
					result = PredictionKatz(str1,str2);
				} else {
					result = PredictionAdd(str1,str2);
				}
			}




			//Without POS
		} else {
			if (withKatz) {
				result = PredictionKatz(str1,str2);
			} else {
				result = PredictionAdd(str1,str2);
			}
		}
		return result;
	}

	/**
	 * The function predicts the next word considering POS tagging(SMOOTHING TECHNIQUE: ADDITIVE SMOOTHING WITH GAMMA=1)
	 *
	 * @param str1 str1 and str2 form the 2-gram (history from the user)
	 * @param str2 str1 and str2 form the 2-gram (history from the user)
	 * @param NextPOS the next POS
	 * @return a list of suggested words in descending order of probability
	 */
	public static String[] PredictionAddPOS(String str1,String str2, String NextPOS) {
		String[] WordList = new String[n_suggestions];
		int[] CountList = new int[n_suggestions];
		String[] WordListPOS = new String[n_suggestions];
		int[] CountListPOS = new int[n_suggestions];
		int Size = 0;
		Random random = new Random();

		try{

			int N = Dictionary.getWordSize();               //Size of the dict
			int WWIndex = Dictionary.getWWIndex(str1,str2); //Index of 2-gram
			Integer NGramCount = 0;							//Count of the 3-gram


			//Firstly, consider only 3-grams that appear in the corpus
			for(int i = 0; i < N; i++){
				String PWord = Dictionary.getIndexWord(i); //Third word
				Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,i);
				NGramCount = Ngram.wordgramMat.get(key);

				//If the 3-gram exists in corpus
				if (NGramCount != null){
					//WordList is not filled -> it keeps adding words
					if (Size < n_suggestions) {
						WordList[Size] = PWord;
						CountList[Size] = NGramCount;
						Size = Size + 1;
						//WordList is filled -> it prioritizes words that have a higher frequency in the corpus
					} else {
						//Select the minimum value in CountList
						int Min = Integer.MAX_VALUE;
						int MinIndex = 0;
						for(int j = 0; j < n_suggestions; j++){
							if (CountList[j] < Min) {
								Min = CountList[j];
								MinIndex = j;
							}
						}
						//Replace if count is higher than Min
						//If it is equal, than it will randomly decide to replace the word or not
						if (NGramCount > Min || (NGramCount == Min && random.nextBoolean())) {
							CountList[MinIndex] = NGramCount;
							WordList[MinIndex] = PWord;
						}
					}
				}
			}

			//WordList is not filled -> consider 3-grams that never appeared in the corpus
			//Here it picks the words randomly in the dictionary (their 3-grams have count zero in ngram matrix, so they can be randomly chosen)
			while (Size < n_suggestions) {
				int DictIndex = random.nextInt(N);
				String PWord = Dictionary.getIndexWord(DictIndex); //Third word
				Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,DictIndex);
				NGramCount = Ngram.wordgramMat.get(key);

				if (NGramCount == null){
					WordList[Size] = PWord;
					CountList[Size] = 0;
					Size = Size + 1;
				}
			}

			//WordList in ascending order of counts
			String[] WordList_sort = new String [n_suggestions];
			int[] CountList_sort = new int[n_suggestions];

			for(int i = 0; i < n_suggestions; i++){
				WordList_sort[i] = WordList[i];
				CountList_sort[i] = CountList[i];
			}
			Arrays.sort(CountList_sort);
			for(int i = 0; i < n_suggestions; i++){
				for(int j = 0; j < n_suggestions; j++){
					if(CountList_sort[i] == CountList[j]){
						WordList_sort[i] = WordList[j];
						CountList[j] = -1;
						break;
					}
				}
			}

			//WordList in descending order of counts
			for(int i = 0; i < n_suggestions; i++){
				WordList[i] = WordList_sort[n_suggestions-1-i];
			}


			Size = 0;



			N = Dictionary.getWordSize();               //Size of the dict
			WWIndex = Dictionary.getWWIndex(str1,str2); //Index of 2-gram
			NGramCount = 0;							//Count of the 3-gram


			//Firstly, consider only 3-grams that appear in the corpus
			for(int i = 0; i < N; i++){
				String PWord = Dictionary.getIndexWord(i); //Third word
				Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,i);
				NGramCount = Ngram.wordgramMat.get(key);

				//If the 3-gram exists in corpus and the word has POS NextPOS
				if (Ngram.wordsToPOS.get(PWord) == null) {
					continue;
				}
				if (NGramCount != null && Ngram.wordsToPOS.get(PWord).contains(NextPOS)){
					//WordList is not filled -> it keeps adding words
					if (Size < n_suggestions) {
						WordListPOS[Size] = PWord;
						CountListPOS[Size] = NGramCount;
						Size = Size + 1;
						//WordList is filled -> it prioritizes words that have a higher frequency in the corpus
					} else {
						//Select the minimum value in CountList
						int Min = Integer.MAX_VALUE;
						int MinIndex = 0;
						for(int j = 0; j < n_suggestions; j++){
							if (CountListPOS[j] < Min) {
								Min = CountListPOS[j];
								MinIndex = j;
							}
						}
						//Replace if count is higher than Min
						//If it is equal, than it will randomly decide to replace the word or not
						if (NGramCount > Min || (NGramCount == Min && random.nextBoolean())) {
							CountListPOS[MinIndex] = NGramCount;
							WordListPOS[MinIndex] = PWord;
						}
					}
				}
			}

			//WordList in ascending order of counts
			String[]WordList_sortPOS = new String [n_suggestions];
			int[] CountList_sortPOS = new int[n_suggestions];

			for(int i = 0; i < n_suggestions; i++){
				WordList_sortPOS[i] = WordListPOS[i];
				CountList_sortPOS[i] = CountListPOS[i];
			}
			Arrays.sort(CountList_sortPOS);
			for(int i = 0; i < n_suggestions; i++){
				for(int j = 0; j < n_suggestions; j++){
					if(CountList_sortPOS[i] == CountListPOS[j]){
						WordList_sortPOS[i] = WordListPOS[j];
						CountListPOS[j] = -1;
						break;
					}
				}
			}

			//WordList in descending order of counts
			for(int i = 0; i < n_suggestions; i++){
				WordListPOS[i] = WordList_sortPOS[n_suggestions-1-i];
			}

			//If WordListPOS is not filled, add from WordList
			int za=0;
			while (Size < n_suggestions && za < n_suggestions) {
				if (wordIsNew(WordListPOS,WordList[za])){
					WordListPOS[Size] = WordList[za];
					Size = Size + 1;
				}
				za = za + 1;
			}

		} catch (Exception e) {
			System.out.println("Error when predicting next word!");
			e.printStackTrace();
		}

		//The list returns the suggested words
		//It is not necessary to use probability here-_-!
		return WordListPOS;
	}

	/**
	 * The function predicts the next word considering POS tagging (SMOOTHING TECHNIQUE: KATZ SMOOTHING WITH K=5)
	 *
	 * @param str1 str1 and str2 form the 2-gram (history from the user)
	 * @param str2 str1 and str2 form the 2-gram (history from the user)
	 * @param NextPOS the next POS
	 * @return a list of suggested words in descending order of probability
	 */
	public static String[] PredictionKatzPOS(String str1,String str2, String NextPOS) {
		String[] WordList = new String[n_suggestions];
		double[] ProbList = new double[n_suggestions];
		String[] WordListPOS = new String[n_suggestions];
		double[] ProbListPOS = new double[n_suggestions];
		double ProbWord;
		double Alpha2_SumProbNum = 0;				  //Elements used to compute Alpha
		double Alpha2_SumProbDen = 0;
		double Alpha1_SumProbNum = 0;
		double Alpha1_SumProbDen = 0;
		int Size = 0;
		double Min;
		int MinIndex;
		Random random = new Random();
		int CountWDict = Dictionary.getWordSize();    //Size of the word dictionary
		int WCountW2 = Dictionary.getWordCount(str2); //Count of str2
		double N = 0;								  //Total number of words in corpus

		try {

			int WWIndex = Dictionary.getWWIndex(str1,str2);
			//IF THERE ARE 3-GRAMS
			if (WWIndex != -1) {
				double nk1_3gram = Ngram.reverseWordgramMat.get(par_k + 1).size();
				double n1_3gram = Ngram.reverseWordgramMat.get(1).size();
				int WWCount = Dictionary.getWWCount(str1, str2);

				//Go through the dictionary searching for 3-grams
				for(int i = 0; i < CountWDict; i++){
					String PWord = Dictionary.getIndexWord(i); //Third word
					Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,i);
					Integer NGramCount = Ngram.wordgramMat.get(key);

					//If the 3-gram exists in corpus
					if (NGramCount != null){
						//WordList is not filled -> it keeps adding words
						if (Size < n_suggestions) {
							WordList[Size] = PWord;
							ProbWord = calc_d(3,NGramCount,nk1_3gram,n1_3gram)*(((double)NGramCount)/WWCount);
							ProbList[Size] = ProbWord;
							Size = Size + 1;

							Alpha2_SumProbNum = Alpha2_SumProbNum + ProbWord;
							int WWCountW2W3 = Dictionary.getWWCount(str2, Dictionary.getIndexWord(i));
							Alpha2_SumProbDen = Alpha2_SumProbDen + calc_d(2,WWCountW2W3,nk1_3gram,n1_3gram)*(WWCountW2W3/WCountW2);
						} else {
							ProbWord = calc_d(3,NGramCount,nk1_3gram,n1_3gram)*(((double)NGramCount)/WWCount);

							Alpha2_SumProbNum = Alpha2_SumProbNum + ProbWord;
							int WWCountW2W3 = Dictionary.getWWCount(str2, Dictionary.getIndexWord(i));
							Alpha2_SumProbDen = Alpha2_SumProbDen + calc_d(2,WWCountW2W3,nk1_3gram,n1_3gram)*(((double)WWCountW2W3)/WCountW2);

							//Select the minimum prob in ProbList
							Min = 1000.0;
							MinIndex = 0;
							for(int j = 0; j < n_suggestions; j++){
								if (ProbList[j] < Min) {
									Min = ProbList[j];
									MinIndex = j;
								}
							}
							//Replace if prob is higher than Min
							//If it is equal, than it will randomly decide to replace the word or not
							if (ProbWord > Min || (ProbWord == Min && random.nextBoolean())) {
								ProbList[MinIndex] = ProbWord;
								WordList[MinIndex] = PWord;
							}
						}
					}
				}
			}

			//The counts subtracted from the nonzero counts are distributed among the zero-count (this is the role of Alpha)
			if (Alpha2_SumProbDen == 1.0) Alpha2_SumProbDen = 0.95;
			double Alpha2 = (1 - Alpha2_SumProbNum)/(1 - Alpha2_SumProbDen);

			//Calculating N
			for(Entry<Integer, List<Integer>> e : Dictionary.reverseWordcounts.entrySet() ){
				N = N + e.getKey()*e.getValue().size();
			}
			double nk1_1gram = Dictionary.reverseWordcounts.get(par_k + 1).size();
			double n1_1gram = Dictionary.reverseWordcounts.get(1).size();

			if (Size < n_suggestions) {
				int WIndexW2 = Dictionary.getWordIndex(str2);
				//IF THERE ARE 2-GRAMS
				if (WIndexW2 != -1) {
					double nk1_2gram = Dictionary.reverseWWcounts.get(par_k + 1).size();
					double n1_2gram = Dictionary.reverseWWcounts.get(1).size();

					//Go through the dictionary searching for 2-grams
					for(int i = 0; i < CountWDict; i++){
						String PWord = Dictionary.getIndexWord(i); //Third word
						int NGramCount_int = Dictionary.getWWCount(str2, PWord);

						//If the 2-gram exists in corpus
						if (NGramCount_int != 0){
							//WordList is not filled -> it keeps adding words
							if (Size < n_suggestions) {
								ProbWord = calc_d(2,NGramCount_int,nk1_2gram,n1_2gram)*(((double)NGramCount_int)/WCountW2);

								Alpha1_SumProbNum = Alpha1_SumProbNum + ProbWord;
								int WCountW3 = Dictionary.getWordCount(PWord);
								Alpha1_SumProbDen = Alpha1_SumProbDen + calc_d(1,WCountW3,nk1_1gram,n1_1gram)*(((double)WCountW3)/N);

								if (wordIsNew(WordList,PWord)){
									WordList[Size] = PWord;
									ProbList[Size] = Alpha2*ProbWord;
									Size = Size + 1;
								}
							} else {
								ProbWord = calc_d(2,NGramCount_int,nk1_2gram,n1_2gram)*(((double)NGramCount_int)/WCountW2);

								Alpha1_SumProbNum = Alpha1_SumProbNum + ProbWord;
								int WCountW3 = Dictionary.getWordCount(PWord);
								Alpha1_SumProbDen = Alpha1_SumProbDen + calc_d(1,WCountW3,nk1_1gram,n1_1gram)*(((double)WCountW3)/N);

								if (wordIsNew(WordList,PWord)){
									//Select the minimum prob in ProbList
									Min = 1000.0;
									MinIndex = 0;
									for(int j = 0; j < n_suggestions; j++){
										if (ProbList[j] < Min) {
											Min = ProbList[j];
											MinIndex = j;
										}
									}
									//Replace if prob is higher than Min
									//If it is equal, than it will randomly decide to replace the word or not
									if ((Alpha2*ProbWord) > Min || ((Alpha2*ProbWord) == Min && random.nextBoolean())) {
										ProbList[MinIndex] = Alpha2*ProbWord;
										WordList[MinIndex] = PWord;
									}
								}
							}
						}
					}
				}
			}

			//The counts subtracted from the nonzero counts are distributed among the zero-count (this is the role of Alpha)
			if (Alpha1_SumProbDen == 1.0) Alpha1_SumProbDen = 0.95;
			double Alpha1 = (1 - Alpha1_SumProbNum)/(1 - Alpha1_SumProbDen);

			if (Size < n_suggestions) {
				//Go through the dictionary searching for 1-grams
				for(int i = 0; i < CountWDict; i++){
					String PWord = Dictionary.getIndexWord(i); //Third word
					int NGramCount_int = Dictionary.getWordCount(PWord);
					//WordList is not filled -> it keeps adding words
					if (Size < n_suggestions) {
						if (wordIsNew(WordList,PWord)){
							WordList[Size] = PWord;
							ProbWord = Alpha1 * calc_d(1,NGramCount_int,nk1_1gram,n1_1gram)*(((double)NGramCount_int)/N);
							ProbList[Size] = ProbWord;
							Size = Size + 1;
						}
					} else {
						if (wordIsNew(WordList,PWord)){
							ProbWord = Alpha1 * calc_d(1,NGramCount_int,nk1_1gram,n1_1gram)*(((double)NGramCount_int)/N);

							//Select the minimum prob in ProbList
							Min = 1000.0;
							MinIndex = 0;
							for(int j = 0; j < n_suggestions; j++){
								if (ProbList[j] < Min) {
									Min = ProbList[j];
									MinIndex = j;
								}
							}
							//Replace if prob is higher than Min
							//If it is equal, than it will randomly decide to replace the word or not
							if (ProbWord > Min || (ProbWord == Min && random.nextBoolean())) {
								ProbList[MinIndex] = ProbWord;
								WordList[MinIndex] = PWord;
							}
						}
					}
				}
			}

			//WordList in ascending order of counts
			String[] WordList_sort = new String [n_suggestions];
			double[] ProbList_sort = new double[n_suggestions];

			for(int i = 0; i < n_suggestions; i++){
				WordList_sort[i] = WordList[i];
				ProbList_sort[i] = ProbList[i];
			}
			Arrays.sort(ProbList_sort);
			for(int i = 0; i < n_suggestions; i++){
				for(int j = 0; j < n_suggestions; j++){
					if(ProbList_sort[i] == ProbList[j]){
						WordList_sort[i] = WordList[j];
						ProbList[j] = -1;
						break;
					}
				}
			}

			//WordList in descending order of counts
			for(int i = 0; i < n_suggestions; i++){
				WordList[i] = WordList_sort[n_suggestions-1-i];
			}

			Alpha2_SumProbNum = 0;				  //Elements used to compute Alpha
			Alpha2_SumProbDen = 0;
			Alpha1_SumProbNum = 0;
			Alpha1_SumProbDen = 0;
			Size = 0;
			CountWDict = Dictionary.getWordSize();    //Size of the word dictionary
			WCountW2 = Dictionary.getWordCount(str2); //Count of str2
			N = 0;								  	  //Total number of words in corpus



			WWIndex = Dictionary.getWWIndex(str1,str2);
			//IF THERE ARE 3-GRAMS
			if (WWIndex != -1) {
				double nk1_3gram = Ngram.reverseWordgramMat.get(par_k + 1).size();
				double n1_3gram = Ngram.reverseWordgramMat.get(1).size();
				int WWCount = Dictionary.getWWCount(str1, str2);

				//Go through the dictionary searching for 3-grams
				for(int i = 0; i < CountWDict; i++){
					String PWord = Dictionary.getIndexWord(i); //Third word
					Tuple<Integer, Integer> key= new Tuple<Integer, Integer>(WWIndex,i);
					Integer NGramCount = Ngram.wordgramMat.get(key);

					//If the 3-gram exists in corpus and the word's POS is NextPOS
					if (Ngram.wordsToPOS.get(PWord) == null)
						continue;
					if (NGramCount != null && Ngram.wordsToPOS.get(PWord).contains(NextPOS)){
						//WordList is not filled -> it keeps adding words
						if (Size < n_suggestions) {
							WordListPOS[Size] = PWord;
							ProbWord = calc_d(3,NGramCount,nk1_3gram,n1_3gram)*(((double)NGramCount)/WWCount);
							ProbListPOS[Size] = ProbWord;
							Size = Size + 1;

							Alpha2_SumProbNum = Alpha2_SumProbNum + ProbWord;
							int WWCountW2W3 = Dictionary.getWWCount(str2, Dictionary.getIndexWord(i));
							Alpha2_SumProbDen = Alpha2_SumProbDen + calc_d(2,WWCountW2W3,nk1_3gram,n1_3gram)*(WWCountW2W3/WCountW2);
						} else {
							ProbWord = calc_d(3,NGramCount,nk1_3gram,n1_3gram)*(((double)NGramCount)/WWCount);

							Alpha2_SumProbNum = Alpha2_SumProbNum + ProbWord;
							int WWCountW2W3 = Dictionary.getWWCount(str2, Dictionary.getIndexWord(i));
							Alpha2_SumProbDen = Alpha2_SumProbDen + calc_d(2,WWCountW2W3,nk1_3gram,n1_3gram)*(((double)WWCountW2W3)/WCountW2);

							//Select the minimum prob in ProbList
							Min = 1000.0;
							MinIndex = 0;
							for(int j = 0; j < n_suggestions; j++){
								if (ProbListPOS[j] < Min) {
									Min = ProbListPOS[j];
									MinIndex = j;
								}
							}
							//Replace if prob is higher than Min
							//If it is equal, than it will randomly decide to replace the word or not
							if (ProbWord > Min || (ProbWord == Min && random.nextBoolean())) {
								ProbListPOS[MinIndex] = ProbWord;
								WordListPOS[MinIndex] = PWord;
							}
						}
					}
				}
			}

			//The counts subtracted from the nonzero counts are distributed among the zero-count (this is the role of Alpha)
			if (Alpha2_SumProbDen == 1.0) Alpha2_SumProbDen = 0.95;
			Alpha2 = (1 - Alpha2_SumProbNum)/(1 - Alpha2_SumProbDen);

			//Calculating N
			for(Entry<Integer, List<Integer>> e : Dictionary.reverseWordcounts.entrySet() ){
				N = N + e.getKey()*e.getValue().size();
			}
			nk1_1gram = Dictionary.reverseWordcounts.get(par_k + 1).size();
			n1_1gram = Dictionary.reverseWordcounts.get(1).size();

			if (Size < n_suggestions) {
				int WIndexW2 = Dictionary.getWordIndex(str2);
				//IF THERE ARE 2-GRAMS
				if (WIndexW2 != -1) {
					double nk1_2gram = Dictionary.reverseWWcounts.get(par_k + 1).size();
					double n1_2gram = Dictionary.reverseWWcounts.get(1).size();

					//Go through the dictionary searching for 2-grams
					for(int i = 0; i < CountWDict; i++){
						String PWord = Dictionary.getIndexWord(i); //Third word
						int NGramCount_int = Dictionary.getWWCount(str2, PWord);
						if (Ngram.wordsToPOS.get(PWord) == null)
							continue;
						//If the 2-gram exists in corpus and the word's POS is NextPOS
						if (NGramCount_int != 0 && Ngram.wordsToPOS.get(PWord).contains(NextPOS)){
							//WordList is not filled -> it keeps adding words
							if (Size < n_suggestions) {
								ProbWord = calc_d(2,NGramCount_int,nk1_2gram,n1_2gram)*(((double)NGramCount_int)/WCountW2);

								Alpha1_SumProbNum = Alpha1_SumProbNum + ProbWord;
								int WCountW3 = Dictionary.getWordCount(PWord);
								Alpha1_SumProbDen = Alpha1_SumProbDen + calc_d(1,WCountW3,nk1_1gram,n1_1gram)*(((double)WCountW3)/N);

								if (wordIsNew(WordListPOS,PWord)){
									WordListPOS[Size] = PWord;
									ProbListPOS[Size] = Alpha2*ProbWord;
									Size = Size + 1;
								}
							} else {
								ProbWord = calc_d(2,NGramCount_int,nk1_2gram,n1_2gram)*(((double)NGramCount_int)/WCountW2);

								Alpha1_SumProbNum = Alpha1_SumProbNum + ProbWord;
								int WCountW3 = Dictionary.getWordCount(PWord);
								Alpha1_SumProbDen = Alpha1_SumProbDen + calc_d(1,WCountW3,nk1_1gram,n1_1gram)*(((double)WCountW3)/N);

								if (wordIsNew(WordListPOS,PWord)){
									//Select the minimum prob in ProbList
									Min = 1000.0;
									MinIndex = 0;
									for(int j = 0; j < n_suggestions; j++){
										if (ProbListPOS[j] < Min) {
											Min = ProbListPOS[j];
											MinIndex = j;
										}
									}
									//Replace if prob is higher than Min
									//If it is equal, than it will randomly decide to replace the word or not
									if ((Alpha2*ProbWord) > Min || ((Alpha2*ProbWord) == Min && random.nextBoolean())) {
										ProbListPOS[MinIndex] = Alpha2*ProbWord;
										WordListPOS[MinIndex] = PWord;
									}
								}
							}
						}
					}
				}
			}

			//The counts subtracted from the nonzero counts are distributed among the zero-count (this is the role of Alpha)
			if (Alpha1_SumProbDen == 1.0) Alpha1_SumProbDen = 0.95;
			Alpha1 = (1 - Alpha1_SumProbNum)/(1 - Alpha1_SumProbDen);

			if (Size < n_suggestions) {
				//Go through the dictionary searching for 1-grams
				for(int i = 0; i < CountWDict; i++){
					String PWord = Dictionary.getIndexWord(i); //Third word
					int NGramCount_int = Dictionary.getWordCount(PWord);

					if (Ngram.wordsToPOS.get(PWord) == null)
						continue;
					//If word has POS NextPOS
					if (Ngram.wordsToPOS.get(PWord).contains(NextPOS)){
						//WordList is not filled -> it keeps adding words
						if (Size < n_suggestions) {
							if (wordIsNew(WordListPOS,PWord)){
								WordListPOS[Size] = PWord;
								ProbWord = Alpha1 * calc_d(1,NGramCount_int,nk1_1gram,n1_1gram)*(((double)NGramCount_int)/N);
								ProbListPOS[Size] = ProbWord;
								Size = Size + 1;
							}
						} else {
							if (wordIsNew(WordListPOS,PWord)){
								ProbWord = Alpha1 * calc_d(1,NGramCount_int,nk1_1gram,n1_1gram)*(((double)NGramCount_int)/N);

								//Select the minimum prob in ProbList
								Min = 1000.0;
								MinIndex = 0;
								for(int j = 0; j < n_suggestions; j++){
									if (ProbListPOS[j] < Min) {
										Min = ProbListPOS[j];
										MinIndex = j;
									}
								}
								//Replace if prob is higher than Min
								//If it is equal, than it will randomly decide to replace the word or not
								if (ProbWord > Min || (ProbWord == Min && random.nextBoolean())) {
									ProbListPOS[MinIndex] = ProbWord;
									WordListPOS[MinIndex] = PWord;
								}
							}
						}
					}
				}
			}

			//WordList in ascending order of counts
			String[] WordList_sortPOS = new String [n_suggestions];
			double[] ProbList_sortPOS = new double[n_suggestions];

			for(int i = 0; i < n_suggestions; i++){
				WordList_sortPOS[i] = WordListPOS[i];
				ProbList_sortPOS[i] = ProbListPOS[i];
			}
			Arrays.sort(ProbList_sortPOS);
			for(int i = 0; i < n_suggestions; i++){
				for(int j = 0; j < n_suggestions; j++){
					if(ProbList_sortPOS[i] == ProbListPOS[j]){
						WordList_sortPOS[i] = WordListPOS[j];
						ProbListPOS[j] = -1;
						break;
					}
				}
			}

			//WordList in descending order of counts
			for(int i = 0; i < n_suggestions; i++){
				WordListPOS[i] = WordList_sortPOS[n_suggestions-1-i];
			}

			//If WordListPOS is not filled, add from WordList
			int za=0;
			while (Size < n_suggestions && za < n_suggestions) {
				if (wordIsNew(WordListPOS,WordList[za])){
					WordListPOS[Size] = WordList[za];
					Size = Size + 1;
				}
				za = za + 1;
			}


		} catch (Exception e) {
			System.out.println("Error when predicting next word!");
			e.printStackTrace();
		}


		return WordListPOS;
	}

}
