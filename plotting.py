# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 17:20:26 2015

@author: KKalem
"""

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from collections import Counter

onedata = np.genfromtxt('1gramscounts.txt', delimiter=',')
twodata = np.genfromtxt('2gramscounts.txt', delimiter=',')
threedata = np.genfromtxt('3gramscounts.txt', delimiter=',')

data = [onedata,twodata,threedata]

listofsizes =[(100+(100*i)) for i in range(99)] + [(10000+(1000*i)) for i in range(21)]
sizesize = len(listofsizes)

listofbags = []

for datum in onedata:
    listofbags.append(datum[1])
for datum in twodata:
    listofbags.append(datum[1])
for datum in threedata:
    listofbags.append(datum[1])

setofbags = Counter(listofbags).keys()
maxbag = max(setofbags)

listofcounts = []

for datum in onedata:
    listofcounts.append(datum[2])
for datum in twodata:
    listofcounts.append(datum[2])
for datum in threedata:
    listofcounts.append(datum[2])
    
setofcounts = Counter(listofcounts).keys()
maxcount = max(setofcounts)

countsurface3 = np.zeros((maxbag+1,sizesize))
countsurface2 = np.zeros((maxbag+1,sizesize))
countsurface1 = np.zeros((maxbag+1,sizesize))

countsurfaces = [countsurface1, countsurface2, countsurface3]

for dat in data:
    surfaceindex = data.index(dat)
    for row in range(len(dat)):
        size = dat[row][0]
        bag = dat[row][1]
        count = dat[row][2]
        sizeindex = listofsizes.index(size)-1
        
        countsurfaces[surfaceindex][bag][sizeindex] = np.log10(count)
np.savetxt("test0",countsurfaces[0])
"""
d = countsurfaces[0][:28]
row_labels = listofsizes
column_labels = range(int(maxbag+1))[:28]

fig, ax = plt.subplots()
heatmap = ax.pcolor(d, cmap=plt.cm.Blues)

# put the major ticks at the middle of each cell
#ax.set_xticks(np.arange(d.shape[0])+0.5, minor=False)
#ax.set_yticks(np.arange(d.shape[1])+0.5, minor=False)

# want a more natural, table-like display
#ax.invert_yaxis()
#ax.xaxis.tick_top()

ax.set_xticklabels(row_labels, minor=False)
ax.set_yticklabels(column_labels, minor=False)
plt.show()

"""
"""
plt.plot(wlineCount,wd,label="dens")
plt.plot(wlineCount,ww,label="ww")
plt.plot(wlineCount,w,label="w")
plt.plot(wlineCount,wmaxpossible,label="max-3grams")

plt.legend()
plt.show()

"""
