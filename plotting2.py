# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 17:20:26 2015

@author: KKalem
"""

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from collections import Counter

data = np.genfromtxt('size_dens_ww_w_maxnum.txt', delimiter=',')

sizes = data[:,0]
densities = np.abs(data[:,1])
#wwcounts = np.log(np.abs(data[:,2]))
#wcount = np.log(np.abs(data[:,3]))
#maxwww = np.log(np.abs(data[:,4]))

plt.plot(sizes,densities,label="density")
#plt.plot(sizes,wwcounts,label="2gram counts")
#plt.plot(sizes,wcount,label="word counts")
#plt.plot(sizes,maxwww,label="maximum 3gram size")

plt.legend(loc='right')
plt.show()


"""

plt.plot(wlineCount,wd,label="dens")
plt.plot(wlineCount,ww,label="ww")
plt.plot(wlineCount,w,label="w")
plt.plot(wlineCount,wmaxpossible,label="max-3grams")

plt.legend()
plt.show()
"""